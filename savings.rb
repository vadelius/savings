puts "Hur stor första/engångsinsättning vill du göra?"
initial_deposit = gets.chomp.to_i
puts "Hur mycket vill du spara i månaden?"
montlhy_deposit = gets.chomp.to_i
years           =     10

interest         = 1.028
monthly_interest = 1.014

yearly_deposit     = montlhy_deposit*12
yearly_income      = yearly_deposit*monthly_interest
last_year_savings  = initial_deposit

(1..years).each do |year|
  savings = last_year_savings*interest + yearly_income
  yearly_interest_income = savings-last_year_savings-yearly_deposit

  puts "Sparat efter år %3d: %8d Årlig räntevinst på %6d" %
       [year, savings, yearly_interest_income]

  last_year_savings = savings
end